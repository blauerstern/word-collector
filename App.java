import java.awt.event.*;
import java.awt.*;
import java.io.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.Border;

public class App
{
  private static final String symbols = "[^a-z-'A-ZäöüßÄÖÜß]";
  private static final int OLD = 0;
  private static final int NEW = 1;
  private static final int NaW = 2;
  private static final int END = 3;
  private static JScrollPane pane;
  private static JPanel panel;
  private static JLabel known, unknown;
  private static JTextField chosen, meaning;
  private static JButton choose[] = new JButton[END];
  @SuppressWarnings("unchecked")
  private static HashSet<String> dict[] = new HashSet[END];
  private static TreeMap<String, String> translations = new TreeMap<String, String>(
    new Comparator<String>()
    {
      public int compare(String s1, String s2)
      {
        return s1.toUpperCase().compareTo(s2.toUpperCase());
      }
    }
  );
  private static ArrayList<JButton> buttons = new ArrayList<>();
  private static ArrayList<Integer> endLines = new ArrayList<>();
  private static String textFile = "texts/lesson.txt";
  private static String lan = "English";
  private static int focused = 0;
  
  private static void makeWords()
  {
    try (BufferedReader text = new BufferedReader(new FileReader(textFile)))
    {
      TextGetter textGetter = textFile.endsWith(".srt") ? new SrtGetter(text, symbols) : new DefaultGetter(text, symbols);
      textGetter.loadFile(buttons, endLines);
    }
    catch (IOException e)
    {
      System.err.println("Input/Output exception has occurred.");
    }
  }

  private static void colorWords()
  {
    for (JButton button : buttons)
    {
      String word = button.getActionCommand().trim().toLowerCase();
      if (dict[OLD].contains(word))
      {
        button.setBackground(Color.black);
        button.setForeground(Color.white);
      }
      else if (dict[NEW].contains(word))
      {
        button.setBackground(Color.yellow);
        button.setForeground(Color.black);
      }
      else if (dict[NaW].contains(word))
      {
        button.setBackground(Color.black);
        button.setForeground(Color.yellow);
      }
      else
      {
        button.setBackground(Color.blue);
        button.setForeground(Color.white);
      }
    }
  }

  private static void styleWords()
  {
    int i = 0;
    for (JButton button : buttons)
    {
      final int I = i;
      button.setFocusable(false);
      button.setFont(new Font("Helvetica", Font.PLAIN, 14));
      button.addActionListener(
        new ActionListener()
        {
          public void actionPerformed(ActionEvent ae)
          {
            String word = ae.getActionCommand();
            chosen.setText(word);
            meaning.setText(translations.get(word));
            buttons.get(focused).setFont(new Font("Helvetica", Font.PLAIN, 14));
            focused = I;
            button.setFont(new Font("Helvetica", Font.BOLD, 14));
            pane.getVerticalScrollBar().setValue(button.getY());
          }
        }
      );
      ++i;
    }
    colorWords();
  }

  private static Component makeText()
  {
    panel = new JPanel(null);
    pane = new JScrollPane(panel);
    int barWidth = ((Integer) UIManager.get("ScrollBar.width")).intValue() + 1;
    Justify layout = new Justify(683 - barWidth, 10, 5, 5, true);

    makeWords();
    styleWords();

    for (int i = 0, j = 0; i < buttons.size(); ++i)
    {
      if (endLines.get(j) == i)
      {
        layout.newLine();
        ++j;
      }
      layout.write(buttons.get(i));
      panel.add(buttons.get(i));
    }

    panel.setPreferredSize(new Dimension(683 - barWidth, layout.newLine()));
    panel.setBackground(Color.BLACK);

    pane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

    return pane;
  }

  private static Component makeMenu()
  {
    JPanel panel = new JPanel(new GridBagLayout());
    panel.setBackground(Color.BLUE);
    
    chosen = new JTextField();
    meaning = new JTextField();
    known = new JLabel("You know " + dict[OLD].size() + " words.");
    unknown = new JLabel("You're learning " + dict[NEW].size() + " words.");

    buttons.get(focused).doClick();

    meaning.addKeyListener(
      new KeyAdapter()
      {
        public void keyReleased(KeyEvent ke)
        {
          int next = focused + 1 < buttons.size() ? focused + 1 : 0;
          int prev = (focused == 0 ? buttons.size() : focused) - 1;
          int tmp;
          switch (ke.getKeyCode())
          {
            case KeyEvent.VK_ESCAPE:
              choose[NaW].doClick();
              buttons.get(next).doClick();
              break;

            case KeyEvent.VK_UP:
              choose[OLD].doClick();
              buttons.get(next).doClick();
              break;
            
            case KeyEvent.VK_DOWN:
              choose[NEW].doClick();
              buttons.get(next).doClick();
              break;
            
            case KeyEvent.VK_LEFT:
              buttons.get(prev).doClick();
              break;
            
            case KeyEvent.VK_RIGHT:
              buttons.get(next).doClick();
              break;
            
            case KeyEvent.VK_SHIFT:
              tmp = focused + 1 < buttons.size() ? focused + 1 : 0;
              while (buttons.get(tmp).getBackground() != Color.blue && tmp != focused)
                tmp = tmp + 1 < buttons.size() ? tmp + 1 : 0;
              if (tmp != focused)
                buttons.get(tmp).doClick();
              break;
            
            case KeyEvent.VK_ENTER:
              tmp = (focused == 0 ? buttons.size() : focused) - 1;
              while (buttons.get(tmp).getBackground() != Color.blue && tmp != focused)
                tmp = (tmp == 0 ? buttons.size() : tmp) - 1;
              if (tmp != focused)
                buttons.get(tmp).doClick();
              break;
            
            default:
              if (!meaning.getText().isEmpty())
                translations.put(chosen.getText(), meaning.getText());
          }
        }
      }
    );

    chosen.setEditable(false);
    chosen.setHorizontalAlignment(JTextField.CENTER);
    meaning.setHorizontalAlignment(JTextField.CENTER);
    known.setForeground(Color.green);
    unknown.setForeground(Color.red);
    known.setFont(new Font(null, Font.PLAIN, 20));
    unknown.setFont(new Font(null, Font.PLAIN, 20));

    choose[OLD] = new JButton("KNOWN");
    choose[NEW] = new JButton("NEW");
    choose[NaW] = new JButton("NOT A WORD");

    GridBagConstraints gbc = new GridBagConstraints();
    gbc.insets = new Insets(10, 0, 0, 0);
    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.gridy = 0;
    panel.add(chosen, gbc);
    gbc.gridy = 1;
    panel.add(meaning, gbc);

    for (int i = OLD; i < END; ++i)
    {
      final int I = i;
      choose[i].setFocusable(false);
      choose[i].addActionListener(
        new ActionListener()
        {
          public void actionPerformed(ActionEvent ae)
          {
            String str = chosen.getText().trim().toLowerCase();
            for (int j = OLD; j < END; ++j)
              if (I == j)
                dict[j].add(str);
              else
                dict[j].remove(str);
      
            known.setText("You know " + dict[OLD].size() + " words.");
            unknown.setText("You're learning " + dict[NEW].size() + " words.");
            colorWords();
          }
        }
      );

      gbc.gridy = I + 2;
      panel.add(choose[i], gbc);
    }

    gbc.gridy = 5;
    panel.add(known, gbc);
    gbc.gridy = 6;
    panel.add(unknown, gbc);

    return panel;
  }

  private static void saveDictionaries()
  {
    try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(lan + "/dictionaries")))
    {
      for (int i = OLD; i < END; ++i)
        out.writeObject(dict[i]);
    }
    catch (Exception e)
    {
      System.err.println("Saving dictionaries has failed.");
    }

    try (BufferedWriter out = new BufferedWriter(new FileWriter(lan + "/translations.txt")))
    {
      Set<String> keys = translations.keySet();
      for (String key : keys)
        out.append(key + '\t' + translations.get(key) + '\n');
    }
    catch (Exception e)
    {
      System.err.println("Saving translations has failed");
    }
  }

  private static void makeGUI()
  {
    JFrame frame = new JFrame();

    frame.addWindowListener(
      new WindowAdapter()
      {
        public void windowOpened(WindowEvent we)
        {
          meaning.requestFocus();
        }

        public void windowClosing(WindowEvent we)
        {
          saveDictionaries();
        }
      }
    );
    frame.addComponentListener(
      new ComponentAdapter()
      {
        public void componentResized(ComponentEvent ce)
        {
          int barWidth = ((Integer) UIManager.get("ScrollBar.width")).intValue() + 1;
          Justify layout = new Justify(frame.getWidth() / 2 - barWidth, 10, 5, 5, true);

          for (int i = 0, j = 0; i < buttons.size(); ++i)
          {
            if (endLines.get(j) == i)
            {
              layout.newLine();
              ++j;
            }
            layout.write(buttons.get(i));
          }

          panel.setPreferredSize(new Dimension(frame.getWidth() / 2 - barWidth, layout.newLine()));
        }
      }
    );
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setTitle("Word collector");
    frame.setSize(1366, 768);
    frame.setLayout(new GridLayout());
    frame.add(makeText());
    frame.add(makeMenu());
    frame.setVisible(true);
  }

  private static void initDictionaries()
  {
    try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(lan + "/dictionaries")))
    {
      for (int i = OLD; i < END; ++i)
      {
        HashSet tmp = (HashSet) in.readObject();
        dict[i] = new HashSet<>();
        for (Object str : tmp)
          dict[i].add((String) str);
      }
    }
    catch (Exception e)
    {
      System.err.println("Reading dictionaries has failed.");

      for (int i = OLD; i < END; ++i)
        dict[i] = new HashSet<>();

      System.out.println("Empty dictionaries have been created.");
    }
    try (BufferedReader in = new BufferedReader(new FileReader(lan + "/translations.txt")))
    {
      String line = in.readLine();
      while (line != null)
      {
        int delimeter = line.indexOf('\t');
        if (delimeter != -1)
          translations.put(line.substring(0, delimeter), line.substring(delimeter + 1));
        line = in.readLine();
      }
    }
    catch (Exception e)
    {
      System.err.println("Reading translations has failed.");
      System.out.println("Empty set of translations has been created.");
    }
  }
  
  public static void main(String[] args)
  {
    if (args.length > 0)
      textFile = args[0];
    if (args.length > 1)
      lan = args[1];
    
    initDictionaries();
    
    SwingUtilities.invokeLater(
      new Runnable()
      {
        public void run()
        {
          makeGUI();
        }
      }
    );
  }
}
