import javax.swing.*;
import java.util.*;
import java.awt.*;

public class Justify
{
  private final int width;
  private final int margin;
  private final int space;
  private final int leading;
  private final boolean ltr;
  private double x;
  private double y;
  private ArrayList<JButton> buttons;

  public Justify(int width, int margin, int space, int leading, boolean ltr)
  {
    this.width = width;
    this.margin = margin;
    this.space = space;
    this.leading = leading;
    this.ltr = ltr;
    x = margin - space;
    y = margin;
    buttons = new ArrayList<>();
  }

  public int write(JButton button)
  {
    Font font = new Font("Helvetica", Font.BOLD, 14);
    FontMetrics fm = button.getFontMetrics(font);
    Insets ins = button.getInsets();
    Dimension size = new Dimension(fm.stringWidth(button.getText()) + ins.left + ins.right, fm.getHeight() + ins.top + ins.bottom);

    button.setSize(size);

    if (x + space + size.getWidth() + margin > width)
    {
      int l = buttons.size();
      double realSpace = (width - x - margin) / (l - 1) + space;
      if (ltr)
      {
        x = margin;
        for (JButton b : buttons)
        {
          b.setLocation((int) x, (int) y);
          x += b.getWidth() + realSpace;
        }
      }
      else
      {
        x = width - margin;
        for (JButton b : buttons)
        {
          b.setLocation((int) (x - b.getWidth()), (int) y);
          x -= b.getWidth() + realSpace;
        }
      }
      x = margin + size.getWidth();
      y += size.getHeight() + leading;
      buttons.clear();
    }
    else
    {
      x += space + size.getWidth();
    }
    buttons.add(button);

    return (int) y - leading + margin;
  }

  public int newLine()
  {
    if (ltr)
    {
      x = margin;
      for (JButton b : buttons)
      {
        b.setLocation((int) x, (int) y);
        x += b.getWidth() + space;
      }
    }
    else
    {
      x = width - margin;
      for (JButton b : buttons)
      {
        b.setLocation((int) (x - b.getWidth()), (int) y);
        x -= b.getWidth() + space;
      }
    }
    x = margin - space;
    y += buttons.get(0).getHeight() + leading;
    buttons.clear();
    return (int) y - leading + margin;
  }
}