import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JButton;

public abstract class TextGetter
{
  BufferedReader text;
  String symbols;

  public TextGetter(BufferedReader text, String symbols)
  {
    this.text = text;
    this.symbols = symbols;
  }

  public abstract void loadFile(ArrayList<JButton> buttons, ArrayList<Integer> endLines) throws IOException;
}
