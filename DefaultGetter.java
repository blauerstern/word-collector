import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;
import javax.swing.JButton;

public class DefaultGetter extends TextGetter
{
  public DefaultGetter(BufferedReader text, String symbols)
  {
    super(text, symbols);
  }

  public void loadFile(ArrayList<JButton> buttons, ArrayList<Integer> endLines) throws IOException
  {
    String line = text.readLine();
    while (line != null)
    {
      StringTokenizer words = new StringTokenizer(line, "  \t\n\r\f");
      while (words.hasMoreTokens())
      {
        String word = words.nextToken();
        JButton button = new JButton(word);
        button.setActionCommand(word.replaceAll(symbols, "").replaceAll("-", " "));
        buttons.add(button);
      }
      endLines.add(buttons.size());
      line = text.readLine();
    }
  }
}
