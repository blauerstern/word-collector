# Kolekcjoner słów
## Uwagi techniczne
* Kod kompatybilny z Javą 8.
* Ze względu na różne zestawy znaków w różnych językach, kod może nie działać poprawnie bez drobnych dostosowań. Sprawdzono i dostosowano do angielskiego i niemieckiego.
## Zastosowanie
Aplikacja ma ułatwiać naukę języka z tekstem. Zalecany cykl użycia aplikacji jest następujący:
1. Znalezienie interesującego artykułu/książki/filmu/odcinka serialu/piosenki w języku obcym.
1. Znalezenie pliku tekstowego z treścią/transkrypcją materiału z poprzedniego punktu.
1. Przerobienie pliku z poprzedniego punku poprzez
	* ręczne usunięcie informacji innych niż słowa języka obcego (niezalecane).
	* przekonsertowanie pliku na SRT za pomocą narzędzia [Subtitle converter](https://gotranscript.com/subtitle-converter) (zalecane).
1. Stworzenie odpowiedniego folderu, jeśli jeszcze go nie ma (patrz sekcja *Uruchamianie*).
1. Uruchomienie aplikacji (patrz sekcja *Uruchamianie*).
1. Praca z aplikacją (patrz sekcja o tej samej nazwie).
1. Użycie wygenerowanie pliku *translations.txt* w katalogu związanym z wybranym trybem pracy do ekspresowanego utworzenia zestawu fiszek w narzędziu [Quizlet](https://quizlet.com).

## Uruchamianie
Aby uruchomić aplikację należy wpisać w konsoli
```bash
java App
```
Aplikacja domyślnie otwiera plik *lesson.txt* z katalogu *texts* i pracuje w trybie angielskim.

Aby załadować inny plik należy podać ścieżkę do tego pliku jako parametr linii poleceń. Np. załadowanie pliku *przykład.txt* z katalogu *na* wymaga wpisanie w konsoli
```bash
java App na/przykład.txt
```
Wszystkie pliki konfiguracyjne danego trybu pracy są zapisywane w jednym katalogu. Domyślnym katalogiem jest *English*, a służy do pracy w trybie angielskim.

Aby uruchomić aplikację w innym trybie pracy, wystarczy podać nazwę tego trybu jako drugi parametr. Nazwa trybu może być dowolna. Musi jednak zostać w wcześniej utworzony katalog o takiej samej nazwie jak tryb. Przykładowo po utworzeniu katalogu *polski* można załadować plik z poprzedniego punktu w trybie polskim następującym poleceniem.
```bash
java App na/przykład.txt polski
```
## Praca z aplikacją
Po pierwszym, domyślnym uruchomieniu aplikacja powinna wyglądać tak:
![zrzut](zrzut.png)
Okno można rozciągać i zmniejszać.
### Po prawej
Prawy ekran wyświetla wczytany tekst w postaci wyjustowanych przycisków. Zawsze jest jedno aktualnie wybrane słowo (początkowo pierwsze słowo z pliku), które można rozpoznać po pogrubionej czcionce. Ekran można przewijać kółkiem myszy, jeśli nie cały tekst mieści się na ekranie.
### Po lewej
Na lewym ekranie znajdują się kolejno:
* nieedytowalne pole tekstowe z wybranych słowem (niczym hasło słownikowe);
* edytowalne pole pole tekstowe z definicją do wpisania (definicji są zapamiętywanie w pliku *translations.txt*), w którym ciągiem należy mieć migający kursor;
* przycisk, oznaczający słowo jako znane;
* przycisk, oznaczający słowo jako nowe,
* przycisk, oznaczający słowo jako ciąg znaków, którego nie warto się uczyć, jak np. imię albo liczba zapisana cyframi;
* napis, informujący o liczbie znanych słów;
* napis, informujący o liczbie słów do nauki.
### Skróty klawiszowe
Można korzystać z następujących skrótów w edytowalnym polu tekstowym:
| Klawisz          | Działanie                                |
|------------------|------------------------------------------|
| Strzałka w lewo  | Wybierz poprzednie słowo z tekstu        |
| Strzałka w prawo | Wybierz następne słowo z tekstu          |
| Strzałka w górę  | Oznacz wybrane słowo jako znane          |
| Strzałka w dół   | Oznacz wybrane słowo jako nowe           |
| Escape           | Oznacz wybrane słowo jako niewarte nauki |
| Enter            | Wybierz następne nieoznaczone słowo      |
| Shift            | Wybierz poprzednie nieoznaczone słowo    |
