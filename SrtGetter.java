import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.swing.JButton;

public class SrtGetter extends TextGetter
{
  public SrtGetter(BufferedReader text, String symbols)
  {
    super(text, symbols);
  }

  private enum LineType
  {
    NUMBER, TIME, TEXT
  }

  public void loadFile(ArrayList<JButton> buttons, ArrayList<Integer> endLines) throws IOException
  {
    String line = text.readLine();
    LineType type = LineType.NUMBER;
    while (line != null)
    {
      switch(type)
      {
        case NUMBER:
          type = LineType.TIME;
          break;
        
        case TIME:
          type = LineType.TEXT;
          break;
        
        case TEXT:
          if (line.isEmpty())
          {
            type = LineType.NUMBER;
            break;
          }
          line = line.replaceAll("<[^>]*>", "");
          StringTokenizer words = new StringTokenizer(line, "  \t\n\r\f");
          while (words.hasMoreTokens())
          {
            String word = words.nextToken();
            JButton button = new JButton(word);
            button.setActionCommand(word.replaceAll(symbols, "").replaceAll("-", " "));
            buttons.add(button);
          }
          endLines.add(buttons.size());
      }
      line = text.readLine();
    }
  }
}
